%title: Infra Cloud Infomaniak
%author: xavki


 ██████╗ ██████╗ ███╗   ██╗████████╗ █████╗ ██╗███╗   ██╗███████╗██████╗ ███████╗
██╔════╝██╔═══██╗████╗  ██║╚══██╔══╝██╔══██╗██║████╗  ██║██╔════╝██╔══██╗██╔════╝
██║     ██║   ██║██╔██╗ ██║   ██║   ███████║██║██╔██╗ ██║█████╗  ██████╔╝███████╗
██║     ██║   ██║██║╚██╗██║   ██║   ██╔══██║██║██║╚██╗██║██╔══╝  ██╔══██╗╚════██║
╚██████╗╚██████╔╝██║ ╚████║   ██║   ██║  ██║██║██║ ╚████║███████╗██║  ██║███████║
 ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚══════╝



-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

History : several attempts

  * 1979 : chroot (Unix v7)

  * 2000 : freebsd jails

  * 2001 : Linux Vserver

  * 2004 : Solaris Containers

  * 2005 : OpenVZ

  * 2006 : Process Container > cgroups by Google

  * 2008 : LVC > cgroups & namespaces

  * 2013 : Docker > Let's go to the hype !!!

  * 2014 : Kubernetes annonced & created

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Why ?

  * Simplify deployments

  * New deliverable mode (based on filesystem tree, metadata)

  * One package with all dependencies (apps, librairies, statics...)

  * Easy to version whole package in an "image"

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Containers vs Virtual Machines ?

  container > server + OS/Kernel + Container Engine + Container (app+lib)

  vm > server + hypervisor OS + virtual server + kernel + apps/libs

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Be careful !! Image vs Container

  The deliverable is not a container, this is the image !!!

Image = code + dependencies + metadatas

Container = running image (1 process) + isolation (cgroups/namespaces)

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Cgroups & Namespaces ?

  Cgroups : allocate resources > cpu, ram, io...

  Namespaces : isolation > pid / net / mount / ipc / uts

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Container runtime : high vs low level

  docker = docker cli + docker engine + containerd + runC

  runC = low level container runtime (just run a container)

-----------------------------------------------------------------------------------------------------------

# Containers : Introduction - what, why, how ?

<br>

Current tasks with containers management :

  * create an image (build)

  * run a container (launch an image)

  * configure/mount the container environment

  * troubleshooting

  * orchestrate it (ex : docker-compose) : create a stack and describe it

  * clusterize it (ex : kubernetes) : ensure high availability



