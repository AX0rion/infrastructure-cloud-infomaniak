%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Today :

    * create a new role to manage consul services

    * generate a file for each service

    * how to remove a service ? (ansible drawbacks)

    * reload the service

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

What do services allow us ?

    * a new dns

    * a load balancing over dns

    * a check integrated in whole consul use case (dns, LB, metrics, alerts...)

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Check if consul exists

```
- name: check if consul exists
  stat:
    path: /usr/local/bin/consul
  register: __consul_installed
```

<br>

Add an assert

```
- name: check prerequites
  assert:
    that:
      - __consul_installed.stat.exists == true
    fail_msg: "Failed : you need to install consul before. Add the role please."
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Add service settings

```
- name: render services
  template:
    src: service_consul.json.j2
    dest: "/etc/consul.d/{{ item.name }}.json"
    mode: 0750
    owner: consul
    group: consul
  with_items: "{{ consul_services }}"
  notify: reload_consul
  when : consul_services
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Add a task to remove service if needed

```
- name: remove a service
  file:
    path: "/etc/consul.d/{{ item }}.json"
    state: absent
  with_items: "{{ consul_service_remove }}"
  when: consul_service_remove is defined
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Add a task to remove service if needed

```
- name: reload consul
  systemd:
    name: consul
    state: reloaded
  when : consul_services
```


-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

The template : port, tags, 

```
{
  "services": [{
    "name": "{{ item.name }}"{% if item.port is defined %},
    "port": {{ item.port }}{% endif %}{% if item.tags is defined %},
    "tags": ["{{ item.tags | join('\",\"') }}"]
    {% endif %}{% if item.check_target is defined %},
    "checks": [{
      "id": "{{ item.name }}",
      "{{ item.type }}": "{{ item.target }}",
      "interval": "{{ item.interval }}"
    }]
    {% endif %}
  }]
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Handlers :

```
- name: reload_consul
  systemd:
    name: consul
    state: reloaded
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Services

<br>

Example :

```
consul_services:
  - {
    name: "openvpn",
    type: "tcp",
    target: "127.0.0.1:1194", 
    interval: "10s", 
    port: 1194
    }
```