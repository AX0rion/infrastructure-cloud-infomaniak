---
# tasks file for roles/mattermost
- name: create group mattermost
  group:
    name: "{{ mattermost_user }}"    
    system: yes
    state: present

- name: create user mattermost
  user:
    name: "{{ mattermost_user }}" 
    system: yes
    shell: /sbin/nologin
    state: present

- name: check if mattermost exists
  stat:
    path: /opt/mattermost/bin/mattermost
  register: __mattermost_exists

- name: if mattermost exists get version
  shell: "cat /etc/systemd/system/mattermost.service | grep Version | sed s/'.*Version '//g"
  register: __get_mattermost_version
  when: __mattermost_exists.stat.exists == true
  changed_when: false

- name: create directory for mattermost data 
  file:
    path: "/opt/mattermost/data"
    state: directory
    mode: 0750
    owner: "{{ mattermost_user }}" 
    group: "{{ mattermost_user }}" 

- name: download mattermost
  unarchive: 
    src: "https://releases.mattermost.com/{{ mattermost_version }}/mattermost-{{ mattermost_version }}-linux-amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
  when: __mattermost_exists.stat.exists == False or not __get_mattermost_version.stdout == mattermost_version

- name: move the binary to the final destination
  copy:
    src: "/tmp/mattermost/"
    dest: "/opt/mattermost"
    owner: "{{ mattermost_user }}"
    group: "{{ mattermost_user }}"
    mode: 0750
    remote_src: yes
  when: __mattermost_exists.stat.exists == False or not __get_mattermost_version.stdout == mattermost_version

- name: mattermost systemd file
  template:
    src: "mattermost.service.j2"
    dest: "/etc/systemd/system/mattermost.service" 
    owner: root
    group: root     
    mode: 0750
  notify: "reload_daemon_and_restart_mattermost"

- name: mattermost configuration file
  template:
    src: "config.json.j2"
    dest: "/opt/mattermost/config/config.json"      
    mode: 0750
    owner: "{{ mattermost_user }}"
    group: "{{ mattermost_user }}" 
  notify: "reload_daemon_and_restart_mattermost"

- meta: flush_handlers

- name: start mattermost
  systemd:
    name: mattermost
    state: started
    enabled: yes
