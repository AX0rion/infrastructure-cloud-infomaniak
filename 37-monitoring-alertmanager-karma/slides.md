%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager

  * trigger on alert (vmalert) to notify

  * can be used in cluster mode

  * GUI

Karma

  * better GUI named karma (and centralized)


2 roles


-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - some variables

```
alertmanager_version: 0.26.0
alertmanager_dir_config: /etc/alertmanager
alertmanager_dir_data: /var/lib/alertmanager
alertmanager_user: alertmanager
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - check if exists and verison

```
- name: check if alertmanager exists
  stat:
    path: /usr/local/bin/alertmanager
  register: __alertmanager_exists
  
- name: if alertmanager exists get version
  shell: "cat /etc/systemd/system/alertmanager.service | grep Version | sed s/'.*Version '//g"
  register: __get_alertmanager_version
  when: __alertmanager_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - check if exists and verison

```
- name: create user alertmanager
  user:
    name: "{{ alertmanager_user }}"
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - install configuration directory

```
- name: create directory for alertmanager configuration
  file:
    path: "{{ item }}"
    state: directory
    mode: 0750
    owner: "{{ alertmanager_user }}"
    group: "{{ alertmanager_user }}"
  loop:
    - "{{ alertmanager_dir_config }}"
    - "{{ alertmanager_dir_data }}"
```



-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - install binaries

```
- name: download alertmanager
  unarchive: 
    src: "https://github.com/prometheus/alertmanager/releases/download/v{{ alertmanager_version }}/alertmanager-{{ alertmanager_version }}.linux-amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
  when: __alertmanager_exists.stat.exists == False or not __get_alertmanager_version.stdout == alertmanager_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - install binaries

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/alertmanager-{{ alertmanager_version }}.linux-amd64/{{ item }}"
    dest: "/usr/local/bin/{{ item }}"
    mode: 0750
    remote_src: yes
    owner: "{{ alertmanager_user }}"
    group: "{{ alertmanager_user }}"
  loop:
  - alertmanager
  - amtool
  when: __alertmanager_exists.stat.exists == False or not __get_alertmanager_version.stdout == alertmanager_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - clean archive

```
- name: clean
  file:
    path: "/tmp/alertmanager-{{ alertmanager_version }}.linux-amd64/{{ item }}"
    state: absent
  loop:
  - alertmanager
  - amtool
  when: __alertmanager_exists.stat.exists == False or not __get_alertmanager_version.stdout == alertmanager_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - install systemd services

```
- name: alertmanager systemd file
  template:
    src: "alertmanager.service.j2"
    dest: "/etc/systemd/system/alertmanager.service"         
    mode: 0750
    owner: "{{ alertmanager_user }}"
    group: "{{ alertmanager_user }}"
  notify: "restart_alertmanager"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - install configuration file

```
- name: alertmanager configuration file
  template:
    src: "alertmanager.yml.j2"
    dest: "/etc/alertmanager/alertmanager.yml"      
    mode: 0750
    owner: "{{ alertmanager_user }}"
    group: "{{ alertmanager_user }}"
  notify: "restart_alertmanager"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - flush handlers and start

```
- meta: flush_handlers

- name: start alertmanager
  systemd:
    name: alertmanager
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - systemd service

```
[Unit]
Description=Alertmanager Version  {{ alertmanager_version }}
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User={{ alertmanager_user }}
Group={{ alertmanager_user }}
WorkingDirectory=/etc/alertmanager/
ExecStart=/usr/local/bin/alertmanager --storage.path={{ alertmanager_dir_data }} --config.file={{ alertmanager_dir_config }}/alertmanager.yml --web.external-url http://0.0.0.0:9093

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - configuration file

```
route:
  group_by: ['alertname']
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 1h
  receiver: 'web.hook'
receivers:
  - name: 'web.hook'
    webhook_configs:
      - url: 'http://127.0.0.1:5001/'
inhibit_rules:
  - source_match:
      severity: 'critical'
    target_match:
      severity: 'warning'
    equal: ['alertname', 'dev', 'instance']
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Alertmanager - handler

```
- name: restart_alertmanager
  systemd:
    name: alertmanager
    state: restarted
    enabled: yes
    daemon_reload: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - variables

```
karma_version: 0.116
karma_dir_config: /etc/karma
karma_user: karma
karma_alertmanager_uri: "http://alertmanager.service.xavki.consul:9093"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - check if exists and version

```
- name: check if karma exists
  stat:
    path: /usr/local/bin/karma
  register: __karma_exists
  
- name: if karma exists get version
  shell: "cat /etc/systemd/system/karma.service | grep Version | sed s/'.*Version '//g"
  register: __get_karma_version
  when: __karma_exists.stat.exists == true
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - create user

```
- name: create user karma
  user:
    name: "{{ karma_user }}"
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - create configuration directory

```
- name: create directory for karma configuration
  file:
    path: "{{ karma_dir_config }}"
    state: directory
    owner: "{{ karma_user }}"
    group: "{{ karma_user }}"
    mode: 0750
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - install binary

```
- name: download karma
  unarchive: 
    src: "https://github.com/prymitive/karma/releases/download/v{{ karma_version }}/karma-linux-amd64.tar.gz"
    dest: /tmp/
    remote_src: yes
  when: __karma_exists.stat.exists == False or not __get_karma_version.stdout == karma_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - install binary

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/karma-linux-amd64"
    dest: "/usr/local/bin/karma"
    mode: 0750
    owner: "{{ karma_user }}"
    group: "{{ karma_user }}"
    remote_src: yes
  when: __karma_exists.stat.exists == False or not __get_karma_version.stdout == karma_version
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - install binary

```
- name: karma systemd file
  template:
    src: "karma.service.j2"
    dest: "/etc/systemd/system/karma.service"         
    mode: 0750
  notify: "restart_karma"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - install configuration file

```
- name: karma configuration file
  template:
    src: "karma.yml.j2"
    dest: "{{ karma_dir_config }}/karma.yaml"      
    mode: 0750
    owner: "{{ karma_user }}"
    group: "{{ karma_user }}"
  notify: "restart_karma"
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - flush handler and start

```
- meta: flush_handlers

- name: start karma
  systemd:
    name: karma
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - the configuration file

```
alertmanager:
  interval: 10s
  servers:
    - name: standard
      uri: {{ karma_alertmanager_uri }}
      timeout: 20s
      proxy: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - the systemd template

```
[Unit]
Description=karma Version  {{ karma_version }}
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User={{ karma_user }}
Group={{ karma_user }}
ExecStart=/usr/local/bin/karma --config.file {{ karma_dir_config  }}/karma.yaml

[Install]
WantedBy=multi-user.target
```

-----------------------------------------------------------------------------------------------------------                                       

# Monitoring - Alertmanager & Karma

<br>

Karma - the handler

```
- name: restart_karma
  systemd:
    name: karma
    state: restarted
    enabled: yes
    daemon_reload: yes
```